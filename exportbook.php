<?php

include_once('export.php');

$xmlFile = './xml/book.xml';
$xslFile = './xsl/book/exportindex_cha.xsl';
$xslFileDetails = './xsl/book/exportdetails_cha.xsl';
$destDir = '../htmlresult/book/';
$mainNode = 'book';
$mainNodeList = 'booklist';
$rootNodeName = 'bookinfo';
$nbItemsPerPage = 20;

$xml = new DOMDocument;
$xml->load($xmlFile);

$xsl = new DOMDocument;
$xsl->load("./xsl/sortbytitle.xsl");

$processor = new XSLTProcessor;
$processor->importStylesheet($xsl);

$doc = $processor->transformToDoc($xml);

processExportWithXML($doc, $xmlFile, $xslFile, $xslFileDetails, $destDir, $mainNode, $mainNodeList, $rootNodeName, $nbItemsPerPage);

copyCommons("./common/book/",$destDir);

?>