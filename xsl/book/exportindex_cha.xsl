﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
<xsl:include href="../common_cha.xsl"/>
<xsl:output method="html"/>

<!-- the main template -->
<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="csscha.css" rel="stylesheet" media="screen" type="text/css" />
<link href="cssbook.css" rel="stylesheet" media="screen" type="text/css" />
</head>
<body>
<div id="content-data">
    <xsl:apply-templates select="bookinfo/booklist"/>
    <xsl:apply-templates select="bookinfo/navigation"/> 
    <br/>
    <div class="value"><xsl:value-of select="//@creationdate"/></div>
</div>
</body>
</html>
</xsl:template>

<xsl:template match="booklist">
<div class="static-content">
	<div id="view-content" class="list"> 
	   <table class="table table-hover table-bordered table-striped table-condensed x-collection">
	      <thead>
	      	<tr>
	      		<th class="item-incollection"></th>
	      		<th class='item-title'>Titre</th>
	      		<th class='item-artist'>Auteur</th>
	      		<th class='item-genre'>Genre</th>
	      		<th class='item-discs'>Editeur</th>
	      		<th class='item-tracks'>Langue</th>
	      		<th class='item-releasedate'>Date de sortie</th>
	      		<th class='item-releasedate'>ISBN</th>
	      		<th class='item-rating'>Ma note</th>
	      		<th class='item-added'>Date d'ajout</th>     
	      	</tr> 
	      </thead>
	      <tbody>
			<xsl:for-each select="book">
			<tr class="item" onclick="document.location = 'details/{id}.html';" style="cursor: pointer">
				<td class="item-incollection" width="100px" valign="top">
					<a href="details/{id}.html" class="title-detail">
						<xsl:choose>
							<xsl:when test="thumbfilepath!=''">
								<img class="block-thumbnail" src="images/{id}t.jpg"/>
							</xsl:when>
							<xsl:otherwise>
								<img class="block-thumbnail" src="images/mainitem2.jpg"/>
							</xsl:otherwise>
						</xsl:choose>
					</a>
				</td>
				<td class="item-artist" valign="top">
			    	<a href="details/{id}.html" class="title-detail"><xsl:value-of select="mainsection/title"/></a>
			    </td><td class="item-title" valign="top">
					<xsl:for-each select="mainsection/authors/author">
			      		<xsl:if test="position() > 1"><BR/></xsl:if>
			      		<xsl:value-of select="person/displayname" /> 
			    	</xsl:for-each>
			    </td>
				<td class="item-genre" valign="top">
					<xsl:for-each select="genres/genre">
			      		<xsl:if test="position() > 1"><BR/></xsl:if>
			      		<xsl:value-of select="displayname" /> 
			    	</xsl:for-each>
				</td>
				<td class="item-discs" width="100px" valign="top"><xsl:value-of select="publisher/displayname"/></td>
				<td class="item-tracks" width="100px" valign="top"><xsl:value-of select="language/displayname"/></td>
				<td class="item-releasedate" width="100px" valign="top"><xsl:value-of select="publicationdate/date"/></td>
				<td class="item-label" valign="top"><xsl:value-of select="isbn"/></td>
				<td class="item-rating" valign="top">
					<xsl:call-template name="star">
						<xsl:with-param name="num" select="mainsection/myrating"/>
						<xsl:with-param name="relativePath" select="'./'"/>
					</xsl:call-template> 
				</td>
				<td class="item-added" valign="top">
					<xsl:call-template name="formatDate"><xsl:with-param name="value" select="dateadded/date"/></xsl:call-template> 
				</td></tr>    
			</xsl:for-each>
			</tbody>
		</table>
	</div>   
</div>
</xsl:template> 
</xsl:stylesheet>
