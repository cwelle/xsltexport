﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="../common_cha.xsl"/>
<xsl:output method="html"/>

<xsl:param name="details">false</xsl:param>
<xsl:param name="absolutelinks">false</xsl:param>
<xsl:param name="thumbnails">true</xsl:param>
<xsl:param name="indextablecols">3</xsl:param>
<xsl:param name="thumbshowcaption">true</xsl:param>

<xsl:template match="book">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="exportdetails_cha.css" rel="stylesheet" media="screen" type="text/css" />
</head>
<body>
<!-- the main template -->
<a href="javascript:history.back()" class="toolbar-button appbar-collection-button"><i class="fa fa-chevron-left fa-lg"></i> <span class="toolbar-button-text">&lt; Back</span></a>
<div id="content-data">
    <div class="static-content">
	    <!-- AUTHOR BOX -->
      	<div id="author" >
	        <div class="item-title">
	        	<h1>
	        		<xsl:for-each select="mainsection/authors/author">
			      		<xsl:if test="position() > 1">, </xsl:if>
			      		<xsl:value-of select="person/displayname" /> 
			    	</xsl:for-each>
	        	</h1>
	        </div>
        </div>
      	<!-- END AUTHOR BOX -->
        <div class="clearfix"></div>
        <div class="item-title top-title">
      		<h1>
      			<xsl:value-of select="mainsection/title" /> - <xsl:value-of select="mainsection/subtitle" /> 
      		</h1>
    	</div>
        <!-- COVER -->
    	<div class="item-image">
    		<xsl:choose>
				<xsl:when test="thumbfilepath!=''">
					<img src="./../images/{id}f.jpg"/>
				</xsl:when>
				<xsl:otherwise>
					<img src="./../images/mainitem2.jpg"/>
				</xsl:otherwise>
			</xsl:choose>
        </div>
    	<!-- END COVER -->
        <div id="lightbox-container">
      		<img id="lightbox-img"/>
    	</div>
        <div class="item-detail">
	      	<!-- BARCODE -->
	      	<div class="item-box item-barcodebox">
          		<span class="item-barcode"><img src="./../images/Barcode.png" width="16" height="16"/><b><xsl:value-of select="isbn" /></b></span>
          	</div>
      		<!-- END BARCODE -->
      		<!-- Producer Year -->
      		<div class="item-box">
        		<h4 class="item-produceryear">
        			<xsl:value-of select="publisher/displayname" /> (<xsl:call-template name="formatDate2"><xsl:with-param name="value" select="publicationdate/date"/></xsl:call-template>)
        		</h4>
		    </div>
      		<!-- END Producer Year -->
		    <!-- FORMATLANGUAGE -->
      		<div class="item-box lightColor">
        		<xsl:value-of select="format/displayname" /> / <xsl:value-of select="mainsection/pagecount" /> pages / <xsl:value-of select="language/displayname" />
        	</div>
      		<!-- END FORMATLANGUAGE -->
	     </div>
         <!-- PERSONAL -->
  	 	<div class="item-personalbox">
        	<div class="item-myrating">
        		<div class="item-myrating">
	        		<xsl:call-template name="star">
	        			<xsl:with-param name="num" select="mainsection/myrating"/>
	        			<xsl:with-param name="relativePath" select="'./../'"/>
	        		</xsl:call-template>
	       		</div>
            </div>
      		<table>
	            <tr class="personal"><th>Quantité</th><td> <span><xsl:value-of select="quantity"/></span></td></tr>
	            <tr class="personal"><th>Date d'ajout</th><td> <span><xsl:call-template name="formatDate"><xsl:with-param name="value" select="dateadded/date"/></xsl:call-template></span></td></tr>
            </table>
		</div>
     	<!-- END PERSONAL -->
        <div class="clearfix"></div>
    	<hr class="seperator"/>
    	<h2 class="darkColor">Genres</h2>
    	<div class="item-genrebox col-md-6 col-sm-6">
        	<div class='item-tag'>
        		<xsl:for-each select="genres/genre">
		      		<xsl:if test="position() > 1"><BR/></xsl:if>
		      		<xsl:value-of select="displayname" /> 
		    	</xsl:for-each>
        	</div>      
        </div>
        <div class="clearfix"></div>
        <!-- PLOT -->
        <div class="clearfix"></div>
    	<hr class="seperator"/>
    	<div class="item-plotbox ">
        	<h2 class="darkColor">Plot</h2>
        	<p><xsl:value-of select="mainsection/plot" disable-output-escaping="yes"/></p>
        </div>
        <!-- END PLOT -->
        <xsl:apply-templates select="links">
    		<xsl:with-param name="id"><xsl:value-of select="id"/></xsl:with-param>
    	</xsl:apply-templates>
    </div>
<!--End Content-->
</div>
<!--End Wrap-->
</body>
</html>
</xsl:template>

<!-- List of tracks -->
<xsl:template match="details/detail">
	<div class="clearfix"></div>
    <hr class="seperator"/>
    <div class="disc-box">
	    <h2 class="darkColor">Piste(s)</h2>
        <table class="table table-hover table-striped table-condensed x-collection">
	        <xsl:for-each select="details/detail">
	              <tr>
	                <td class="rank lightColor"><xsl:value-of select="position"/></td>
	                <td class="track"><xsl:value-of select="title"/></td>
	                <td class="length lightColor"><xsl:if test="length!='00:00'"><span class="item-length"><xsl:value-of select="length"/></span></xsl:if></td>
	              </tr>
	    	</xsl:for-each>
         </table>
    </div>
</xsl:template>

<!-- cover -->
<xsl:template name="cover">
	<xsl:param name="in_viewhref"/>
  	<xsl:param name="in_cover"/>
  	<xsl:param name="in_id"/>
  	<xsl:param name="in_postfix"/>
  	<xsl:param name="classname" /> 
  
  <xsl:choose>
		<xsl:when test="$templatetype='view'">
		  <a href="{$in_viewhref}">
		 		<img src="file:///{$in_cover}" border="0" class="{$classname}"/>
		  </a>
		</xsl:when>
		<xsl:otherwise>
		  <xsl:choose>
			  <xsl:when test="$absolutelinks = 'true'">
			 		<a href="file:///$in_cover"><img src="file:///{$in_cover}" class="{$classname}"/></a>
			  </xsl:when>
			  <xsl:otherwise>
			 		<xsl:variable name="extf"><xsl:call-template name="extractfileextension"><xsl:with-param name="filepath" select="$in_cover"/></xsl:call-template></xsl:variable>
			 		<a href="../images/{$in_id}{$in_postfix}.{$extf}"><img src="../images/{$in_id}{$in_postfix}.{$extf}" class="{$classname}"/></a>
			  </xsl:otherwise>
		  </xsl:choose>
		</xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>