﻿<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="../common_cha.xsl"/>
<xsl:output method="html"/>

<xsl:param name="details">false</xsl:param>
<xsl:param name="absolutelinks">false</xsl:param>
<xsl:param name="thumbnails">true</xsl:param>
<xsl:param name="indextablecols">3</xsl:param>
<xsl:param name="thumbshowcaption">true</xsl:param>

<xsl:template match="game">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="exportdetails_cha.css" rel="stylesheet" media="screen" type="text/css" />
</head>
<body>
<!-- the main template -->
<a href="../index.html" class="toolbar-button appbar-collection-button"><i class="fa fa-chevron-left fa-lg"></i> <span class="toolbar-button-text">&lt; Back</span></a>
<div id="content-data">
	<div class="static-content">
        <div class="item-title top-title">
  	    	<h1><xsl:value-of select="title"/></h1>
	    </div>
   		<!-- COVER -->
		<div class="item-image">
	    	<xsl:choose>
				<xsl:when test="thumbfilepath!=''">
					<img src="../images/{id}f.jpg"/>
				</xsl:when>
				<xsl:otherwise>
					<img src="../images/mainitem2.jpg"/>
				</xsl:otherwise>
			</xsl:choose>
      	</div>
		<!-- END COVER -->
        <div id="lightbox-container">
	    	<img id="lightbox-img"/>
	    </div>
    	<div class="item-detail">
      		<!-- EXTRA DETAILS -->
		    <div class="item-title-sub"><span class="item-logo item-platform platform{clzplatformid}" title="{platform/displayname}"><xsl:value-of select="platform/displayname"/></span></div>
  			<h4 class="item-produceryear"><span class="item-studio"><xsl:value-of select="publisher/displayname"/> / <xsl:call-template name="formatDate2"><xsl:with-param name="value" select="releasedate/date"/></xsl:call-template></span></h4>
	        <div class="item-genre lightColor">
	        	<xsl:for-each select="genres/genre">
		      		<xsl:if test="position() > 1"><BR/></xsl:if>
		      		<xsl:value-of select="displayname" /> 
		    	</xsl:for-each>
			</div>
	        <div class="item-details3">
	        	<xsl:value-of select="format/displayname"/>
        	</div>
            <!-- END EXTRA DETAILS -->
       </div>
       	<!-- PERSONAL -->
  	 	<div class="item-personalbox">
        	<div class="item-myrating">
        		<div class="item-myrating">
	        		<xsl:call-template name="star">
	        			<xsl:with-param name="num" select="myrating"/>
	        			<xsl:with-param name="relativePath" select="'./../'"/>
	        		</xsl:call-template>
	       		</div>
            </div>
      		<table>
	            <tr class="personal"><th>Quantité</th><td> <span><xsl:value-of select="quantity"/></span></td></tr>
	            <tr class="personal"><th>Date d'ajout</th><td> <span><xsl:call-template name="formatDate"><xsl:with-param name="value" select="dateadded/date"/></xsl:call-template></span></td></tr>
            </table>
		</div>
     	<!-- END PERSONAL -->
        <!-- PLOT -->
		<div class="clearfix"></div>
		<hr class="seperator"/>
        <div class="item-plotbox ">
    		<h2 class="darkColor">Description</h2>
        	<p><xsl:value-of select="description" disable-output-escaping="yes"/></p>
		</div>
    	<!-- END PLOT -->
    	<xsl:apply-templates select="links">
    		<xsl:with-param name="id"><xsl:value-of select="id"/></xsl:with-param>
    	</xsl:apply-templates>
    </div>
	<!--End Content-->
</div>
<!--End Wrap-->
</body>
</html>
</xsl:template>
</xsl:stylesheet>