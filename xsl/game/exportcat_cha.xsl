<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:php="http://php.net/xsl">
<xsl:include href="../common_cha.xsl"/>
<xsl:output method="html"/>
<!-- the main template -->
<xsl:template match="/">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="exportindex_cha.css" rel="stylesheet" media="screen" type="text/css" />
</head>
<body>
<div id="content-data">
    <xsl:apply-templates select="catlist"/>
    <br/>
    <div class="value"><xsl:value-of select="//@creationdate"/></div>
</div>
</body>
</html>
</xsl:template>

<xsl:template match="catlist">
	<xsl:for-each select="cat">
        <a href="{@url}" target="tabIframeList"><xsl:value-of select="@name"/></a><BR/>
    </xsl:for-each>
    <xsl:for-each select="cat">
	  <xsl:if test="position() = 1">
	    <iframe class="tabContent" width="100%" height="100%" name="tabIframeList" src="{@url}" marginheight="8" marginwidth="8" frameborder="0"></iframe>
	  </xsl:if>
	</xsl:for-each>
</xsl:template> 
</xsl:stylesheet>
