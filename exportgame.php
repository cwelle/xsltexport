<?php

class platform{
	public $name;
	public $id;
	public $key;
}

include_once('export.php');

$xmlFile = './xml/game.xml';
$xslFile = './xsl/game/exportindex_cha.xsl';
$xslFileDetails = './xsl/game/exportdetails_cha.xsl';
$destDir = '../htmlresult/game/';
$mainNode = 'game';
$mainNodeList = 'gamelist';
$rootNodeName = 'gameinfo';
$nbItemsPerPage = 20;

// first: split into plateform
$xml = new DOMDocument;
$xml->load($xmlFile);

$items = $xml->getElementsByTagName("clzplatformid");
$result;
foreach ($items as $item) {
	$id = $item->nodeValue;
	if ($id >= 0) {
		$parent = $item->parentNode;
		$plat = new platform;
		$plat->id = $id;
		$name = $parent->getElementsByTagName("platform")->item(0);
		if ($name != null) {
			$name = $name->getElementsByTagName("displayname")->item(0);
			if ($name != null) {
				$name = $name->nodeValue;
			} else {
				$name = "None";
			}	
		} else {
			$name = "None";
		}
		$key = cleanString($name);
		$plat->key = $key;
		$plat->name = $name;
		$result[$key] = $plat;
	}
}

ksort($result);


$xsl = new DOMDocument;
$xsl->load("./xsl/sortbytitle.xsl");

$processor = new XSLTProcessor;
$processor->importStylesheet($xsl);

$doc = $processor->transformToDoc($xml);
$cat;
foreach ($result as $key => $value) {
	// for each platform
	$platform = $key;
	$destDirDetails = '../htmlresult/game/'.$platform.'/';
	$proc = new XSLTProcessor;
	$cat[$value->name] = "./".$platform."/index.html";
	$proc->setParameter('', 'platform', $value->id);

	processExportWithXML($doc, $xmlFile, $xslFile, $xslFileDetails, $destDirDetails, $mainNode, $mainNodeList, $rootNodeName, $nbItemsPerPage, $proc);
}


// create main index
$mainxml = getCatDom($cat);
// Load XSL file
$xsl = new DOMDocument;
$xsl->load('./xsl/game/exportcat_cha.xsl');

// Configure the transformer
$proc = new XSLTProcessor;

// Attach the xsl rules
$proc->importStyleSheet($xsl);
if ($xml_output = $proc->transformToXML($mainxml)) {
    file_put_contents($destDir.'index.html', utf8_encode(trim($xml_output)));
} else {
  	trigger_error('Oops, XSLT transformation failed!', E_USER_ERROR);
} 

copyCommons("./common/game/",$destDir);


?>