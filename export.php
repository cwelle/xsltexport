<?php

include('functions.php');

ini_set('max_execution_time', 3000000);

function exportDetailsHTML($xmlFile, $xslFileDetails, $destDir, $mainNode, $mainNodeList, $emptyDoc) {
	$destDirDetails = $destDir."details/";
	if (!file_exists($destDirDetails)) {
		mkdir($destDirDetails, 0777, true);
	}
	$destDirImage = $destDir."images/";
	if (!file_exists($destDirImage)) {
		mkdir($destDirImage, 0777, true);
	}
	$destDirFiles = $destDir."files/";
	if (!file_exists($destDirFiles)) {
		mkdir($destDirFiles, 0777, true);
	}
	
	$xml = new DOMDocument;
	$xml->load($xmlFile);
	
	$xslDetails = new DOMDocument;
	$xslDetails->load($xslFileDetails);
	$processor = new XSLTProcessor;
	$processor->importStylesheet($xslDetails);
	$items = $xml->getElementsByTagName($mainNode);
	foreach ($items as $item) {
		// get id
		$idNodes = $item->getElementsByTagName('id');
		// get right id
		$id;
		foreach ($idNodes as $idNode) {
			$parentOfNode = $idNode->parentNode;
			$nodeTagName = $parentOfNode->tagName;
			if ($nodeTagName == $mainNode) {
				$id = $idNode->nodeValue;
			}
		}
		
		$doc = new DOMDocument;
		$doc->appendChild($doc->importNode($emptyDoc->firstChild, true));
		$parentNode = $doc->getElementsByTagName($mainNodeList);
		$parentNode->item(0)->appendChild($doc->importNode($item, true));
		$doc->saveXML();
		
		if ($xml_output = trim($processor->transformToXML($doc))) {
		    file_put_contents($destDirDetails.$id.'.html', utf8_encode(trim($xml_output)));
		} else {
		  	trigger_error('Oops, XSLT transformation failed!', E_USER_ERROR);
		} 
		copyFiles($item, $destDirImage, $destDirFiles, $id);
	}
}

function copyFiles($item, $destDirImage, $destDirFiles, $id) {
	// copy all images and thumbnail
	$coverfronts = $item->getElementsByTagName('coverfront');
	if ($coverfronts->length > 0) {
		$coverfront = $coverfronts->item(0);
	    $img = $coverfront->nodeValue;
		// copy image
		copy($img, $destDirImage.$id."f.jpg");
		echo("Copy ".$img." to ".$destDirImage.$id."f.jpg<BR/>");
		// create a thumbnail
		createThumb($img,$destDirImage, $id,100,"t");
	}
	// copy personal screenshots
	$screenshots = $item->getElementsByTagName('links');
	if ($screenshots->length > 0) {
		$screenshots = $screenshots->item(0)->getElementsByTagName('link');
		if ($screenshots->length > 0) {
			$destDirImagePerso = $destDirImage.$id."/";
			foreach ($screenshots as $screenshot) {
				$type = $screenshot->getElementsByTagName("urltype");
				if ($type->length > 0) {
					$type = $type->item(0)->nodeValue;
					if ($type == "Image") {
						$screenshot = $screenshot->getElementsByTagName("url")->item(0);
					    $img = $screenshot->nodeValue;
						// copy image
						if (!file_exists($destDirImagePerso)) {
							mkdir($destDirImagePerso, 0777, true);
						}
						$ext = pathinfo($img, PATHINFO_EXTENSION);
						$filename = basename($img, ".".$ext);
						copy($img, $destDirImagePerso.$filename.".".$ext);
						// create a thumbnail
						createThumb($img,$destDirImagePerso, $filename,100,"t");
					}
				}
			}
		}
	}
	// copy all mp3 and create playlist.xml file
	$Nodes = $item->getElementsByTagName("detail");	
	if ($Nodes->length > 0) {
		foreach ($Nodes as $Node) {
			$type = $Node->getAttribute("type");
			if ($type == 'disc') {
				$cdId = $Node->getElementsByTagName("index")->item(0)->nodeValue;
				$CDNodes = $Node->getElementsByTagName("detail");
				$docPlaylist = new DOMDocument('1.0', 'UTF-8');
				$root = $docPlaylist->createElement('playlist', "");
				$root->setAttribute("version", "1");
				$root->setAttribute("xmlns", "http://xspf.org/ns/0/");
				$docPlaylist->appendChild($root);	
				$titleNode = $docPlaylist->createElement("title", "Playlist");
				$root->appendChild($titleNode);	
				$creatorNode = $docPlaylist->createElement("creator", "Cha");
				$root->appendChild($creatorNode);	
				$linkNode = $docPlaylist->createElement("link", "");
				$root->appendChild($linkNode);	
				$infoNode = $docPlaylist->createElement("info", "Playlist");
				$root->appendChild($infoNode);	
				$imageNode = $docPlaylist->createElement("image", "");
				$root->appendChild($imageNode);	
				$trackListNode = $docPlaylist->createElement("trackList", "");
				$root->appendChild($trackListNode);	
				foreach ($CDNodes as $CDNode) {
					$mp3files = $CDNode->getElementsByTagName('audiofile');
					$destDirFilesId = $destDirFiles.$id."/cd".$cdId."/";
					if ($mp3files->length > 0) {
						foreach ($mp3files as $mp3file) {
							if (!file_exists($destDirFilesId)) {
								mkdir($destDirFilesId, 0777, true);
							}
							$parentNode = $mp3file->parentNode;
							$indexTrack = $parentNode->getElementsByTagName("index")->item(0)->nodeValue;
							$trackNode = $docPlaylist->createElement("track", "");
							$trackListNode->appendChild($trackNode);
						    $file = $mp3file->nodeValue;
							// copy file
							$stringM3U = $destDirFilesId.$indexTrack.".mp3";
							// Copy only if not exist or file newer or bigger
							if (file_exists($stringM3U)) {
								// file newer or bigger ?
								if (filesize($file) > filesize($stringM3U)) {
									copy(urldecode($file), $stringM3U);
								}
							} else  {
								copy(urldecode($file), $stringM3U);
							}
							$location = $docPlaylist->createElement("location", "./../files/".$id."/cd".$cdId."/".$indexTrack.".mp3");
							$trackNode->appendChild($location);
							$creator = $docPlaylist->createElement("creator", "Cha");
							$trackNode->appendChild($creator);
							$album = $docPlaylist->createElement("album", "");
							$trackNode->appendChild($album);
							$title = $docPlaylist->createElement("title", $parentNode->getElementsByTagName("title")->item(0)->nodeValue);
							$trackNode->appendChild($title);
							$annotation = $docPlaylist->createElement("annotation", "");
							$trackNode->appendChild($annotation);
							$duration = $docPlaylist->createElement("duration", "");
							$trackNode->appendChild($duration);
							$image = $docPlaylist->createElement("image", "");
							$trackNode->appendChild($image);
							$info = $docPlaylist->createElement("info", "");
							$trackNode->appendChild($info);
							$link = $docPlaylist->createElement("link", "");
							$trackNode->appendChild($link);
						}
						if (file_exists($destDirFilesId)) {
						    file_put_contents($destDirFilesId.'playlist.xml', utf8_encode(trim($docPlaylist->saveXML())));
						}
					}
				}
			}
		}
	}
}

function processExport($xmlFile, $xslFile, $xslFileDetails, $destDir, $mainNode, $mainNodeList, $rootNodeName, $nbItemsPerPage, $proc = null) {
	// Load XML file
	$xml = new DOMDocument;
	$xml->load($xmlFile);
	
	processExportWithXML($xml, $xmlFile, $xslFile, $xslFileDetails, $destDir, $mainNode, $mainNodeList, $rootNodeName, $nbItemsPerPage, $proc);
}

function processExportWithXML($xml,$xmlFile, $xslFile, $xslFileDetails, $destDir, $mainNode, $mainNodeList, $rootNodeName, $nbItemsPerPage, $proc = null) {
	$emptyDoc = createEmptyRoot($xmlFile, $mainNode, $rootNodeName);
	$result = filterXML($xml, $mainNode, $proc);
	$numberOfItems = count($result);
	if ($numberOfItems > $nbItemsPerPage) {
		// Split and create for each page a DOMDocument XML
		$numberOfPages = ceil($numberOfItems/$nbItemsPerPage);
		for ($i=0; $i < $numberOfPages; $i++) {
			$page = createEmptyRoot($xmlFile, $mainNode, $rootNodeName);
			// copy items to xml
			$nbToCopy = $nbItemsPerPage*($i+1);
			if ($nbToCopy>$numberOfItems) {
				$nbToCopy = $numberOfItems;
			}
			$rootNode = $page->getElementsByTagName($mainNodeList)->item(0);
			for ($j = ($nbItemsPerPage*$i); $j < $nbToCopy; $j++) {
				$currentNode = $result[$j];
				$rootNode->appendChild($page->importNode($currentNode, true));
			}
					
			// add navigation node
			$rootNode = $page->getElementsByTagName($rootNodeName)->item(0);
			$docNav = adjustNavigation($page, $i, $numberOfPages);
			$nav = $docNav->getElementsByTagName('navigation')->item(0);
			$rootNode->appendChild($page->importNode($nav, true));
			$page->saveXML();
			processExportIndexes($page, $xslFile, $destDir, $i); 
		}
	} else {
		$page = createEmptyRoot($xmlFile, $mainNode, $rootNodeName);
		$rootNode = $page->getElementsByTagName($mainNodeList)->item(0);
		foreach ($result as $node) {
			$rootNode->appendChild($page->importNode($node, true));
		}
		processExportIndexes($page, $xslFile, $destDir);
	}
	exportDetailsHTML($xmlFile, $xslFileDetails, $destDir, $mainNode, $mainNodeList, $emptyDoc);
	
	copyCommons("./common/common/",$destDir);
	
	echo "JOB'S DONE!<BR>";
}

function filterXML($xml, $mainNode, $proc = null) {
	$items=$xml->getElementsByTagName($mainNode);
	if ($proc != null) {
		$platformId = $proc->getParameter('', 'platform');
		foreach ($items as $item) {
			$nodes = $item->getElementsByTagName("clzplatformid");
			if ($nodes != null) {
				foreach ($nodes as $node) {
					$id = $node->nodeValue;
					if ($id == $platformId) {
						$result[] = $item;
						break;
					}
				}
			}
		}
	} else {
		foreach ($items as $item) {
			$result[] = $item;
		}
	}
	
	return $result;
}

function processExportIndexes($xml, $xslFile, $destDir, $index = 0) {
	if (!file_exists($destDir)) {
		mkdir($destDir, 0777, true);
	}
	
	// Load XSL file
	$xsl = new DOMDocument;
	$xsl->load($xslFile);

	// Configure the transformer
	$proc = new XSLTProcessor;

	// Attach the xsl rules
	$proc->importStyleSheet($xsl);
	// Process indexXXX.html
	if ($xml_output = trim($proc->transformToXML($xml))) {
		if ($index == 0) {
		    file_put_contents($destDir.'index.html', utf8_encode(trim($xml_output)));
		} else {
		    file_put_contents($destDir.'index'.$index.'.html', utf8_encode(trim($xml_output)));
		}
	} else {
	  	trigger_error('Oops, XSLT transformation failed!', E_USER_ERROR);
	} 
}
?>