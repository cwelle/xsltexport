<?php

include_once('export.php');

$xmlFile = './xml/music.xml';
$xslFile = './xsl/music/exportindex_cha.xsl';
$xslFileDetails = './xsl/music/exportdetails_cha.xsl';
$destDir = '../htmlresult/music/';
$mainNode = 'music';
$mainNodeList = 'musiclist';
$rootNodeName = 'musicinfo';
$nbItemsPerPage = 20;

$xml = new DOMDocument;
$xml->load($xmlFile);

$xsl = new DOMDocument;
$xsl->load("./xsl/sortbytitle.xsl");

$processor = new XSLTProcessor;
$processor->importStylesheet($xsl);

$doc = $processor->transformToDoc($xml);

processExportWithXML($doc, $xmlFile, $xslFile, $xslFileDetails, $destDir, $mainNode, $mainNodeList, $rootNodeName, $nbItemsPerPage);

copyCommons("./common/music/",$destDir);

?>