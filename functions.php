<?php
 
function createThumb( $image, $pathToThumbs, $fname, $thumbWidth, $suffix ) {
    // parse path for the extension
    $info = pathinfo($image);
    // continue only if this is a JPEG image
    if ( strtolower($info['extension']) == 'jpg' || strtolower($info['extension']) == 'jpeg') {
      echo "Creating thumbnail for {$image} <br />";

      // load image and get image size
      $img = imagecreatefromjpeg( "{$image}" );
      $width = imagesx( $img );
      $height = imagesy( $img );

      // calculate thumbnail size
      $new_width = $thumbWidth;
      $new_height = floor( $height * ( $thumbWidth / $width ) );

      // create a new temporary image
      $tmp_img = imagecreatetruecolor( $new_width, $new_height );

      // copy and resize old image into new image 
      imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

      // save thumbnail into a file
      imagejpeg( $tmp_img, "{$pathToThumbs}{$fname}{$suffix}.jpg" );
    } else if ( strtolower($info['extension']) == 'png') {
      	echo "Creating thumbnail for {$image} <br />";
		resize_png("{$image}", "{$pathToThumbs}{$fname}{$suffix}.jpg", $thumbWidth);
    } else if ( strtolower($info['extension']) == 'gif') {
      	echo "Creating thumbnail for {$image} <br />";
		resize_gif("{$image}", "{$pathToThumbs}{$fname}{$suffix}.jpg", $thumbWidth);
    }
}

function resize_gif($src,$dst, $thumbWidth) {
	$img = imagecreatefromgif($src);
  	$width = imagesx( $img );
  	$height = imagesy( $img );

  	// calculate thumbnail size
  	$new_width = $thumbWidth;
  	$new_height = floor( $height * ( $thumbWidth / $width ) );
	
    // create a new temporary image
    $tmp_img = imagecreatetruecolor( $new_width, $new_height );

    // copy and resize old image into new image 
    imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );

    // save thumbnail into a file
    imagejpeg($tmp_img, $dst );
	imagedestroy($tmp_img);
}

function resize_png($src,$dst, $thumbWidth) {
	$img = imagecreatefrompng($src);
  	$width = imagesx( $img );
  	$height = imagesy( $img );

  	// calculate thumbnail size
  	$new_width = $thumbWidth;
  	$new_height = floor( $height * ( $thumbWidth / $width ) );
	
    $tim = imagecreatetruecolor($new_width,$new_height);
    imagecopyresampled($tim,$img,0,0,0,0,$new_width,$new_height,$width,$height);
    $tim = ImageTrueColorToPalette2($tim,false,255);
	imagejpeg($tim, $dst);
	imagedestroy($tim);
}

function ImageTrueColorToPalette2($image, $dither, $ncolors) {
    $width = imagesx( $image );
    $height = imagesy( $image );
    $colors_handle = ImageCreateTrueColor( $width, $height );
    ImageCopyMerge( $colors_handle, $image, 0, 0, 0, 0, $width, $height, 100 );
    ImageTrueColorToPalette( $image, $dither, $ncolors );
    ImageColorMatch( $colors_handle, $image );
    ImageDestroy($colors_handle);
    return $image;
}


function copyCommons( $pathToSource, $pathToDest ) {
  // open the directory
  $dir = opendir( $pathToSource );
  // loop through it, looking for any/all JPG files:
  while (false !== ($fname = readdir( $dir ))) {
  	if ($fname != '.' && $fname != '..') {
		echo "Copy {$fname} to {$pathToDest}<BR/>";
		smartCopy($pathToSource.$fname, $pathToDest.$fname);
  	}
  }
  // close the directory
  closedir( $dir );
}

function smartCopy($source, $dest, $options=array('folderPermission'=>0755,'filePermission'=>0755)) { 
    $result=false; 
    
    if (is_file($source)) { 
        if ($dest[strlen($dest)-1]=='/') { 
            if (!file_exists($dest)) { 
                cmfcDirectory::makeAll($dest,$options['folderPermission'],true); 
            } 
            $__dest=$dest."/".basename($source); 
        } else { 
            $__dest=$dest; 
        } 
        $result=copy($source, $__dest); 
    } elseif(is_dir($source)) { 
        if ($dest[strlen($dest)-1]=='/') { 
            if ($source[strlen($source)-1]=='/') { 
                //Copy only contents 
            } else { 
                //Change parent itself and its contents 
                $dest=$dest.basename($source); 
                @mkdir($dest); 
            } 
        } else { 
            if ($source[strlen($source)-1]=='/') { 
                //Copy parent directory with new name and all its content 
                @mkdir($dest,$options['folderPermission']); 
            } else { 
                //Copy parent directory with new name and all its content 
                @mkdir($dest,$options['folderPermission']); 
            } 
        } 

        $dirHandle=opendir($source); 
        while($file=readdir($dirHandle)) 
        { 
            if($file!="." && $file!="..") 
            { 
                 if(!is_dir($source."/".$file)) { 
                    $__dest=$dest."/".$file; 
                } else { 
                    $__dest=$dest."/".$file; 
                } 
                //echo "$source/$file ||| $__dest<br />"; 
                $result=smartCopy($source."/".$file, $__dest, $options); 
            } 
        } 
        closedir($dirHandle); 
        
    } else { 
        $result=false; 
    } 
    return $result; 
} 

function createEmptyRoot($xslFile, $mainNode, $rootNode) {
	// create details page
	$doc = new DOMDocument('1.0', 'UTF-8');
	$doc->load($xslFile);
	$featuredde1 = $doc->getElementsByTagName($mainNode);
	$length = $featuredde1->length;
	
	// Iterate backwards by decrementing the loop counter 
	$parent;
	for ($i=$length-1;$i>=0;$i--)
	{
	    $p = $featuredde1->item($i);    
        $parent = $p->parentNode;
        $parent->removeChild($p);        
	}
	$doc->saveXML();
	
	return $doc;
}

function cleanString($string) {
	//Normalisation de la chaine utf8 en mode caractère + accents
	$string = mb_strtolower($string, 'utf-8');
	/** Nettoyage des caractères */
	mb_regex_encoding('utf-8');
	$string = trim(preg_replace('/ +/', ' ', mb_ereg_replace('[^0-9\p{L}]+', ' ', $string)));
	/** strtr() sait gérer le multibyte */
	$string = strtr($string, array(
	'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'a'=>'a', 'a'=>'a', 'a'=>'a', 'ç'=>'c', 'c'=>'c', 'c'=>'c', 'c'=>'c', 'c'=>'c', 'd'=>'d', 'd'=>'d', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'e'=>'e', 'g'=>'g', 'g'=>'g', 'g'=>'g', 'h'=>'h', 'h'=>'h', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'i'=>'i', 'i'=>'i', 'i'=>'i', 'i'=>'i', 'i'=>'i', '?'=>'i', 'j'=>'j', 'k'=>'k', '?'=>'k', 'l'=>'l', 'l'=>'l', 'l'=>'l', '?'=>'l', 'l'=>'l', 'ñ'=>'n', 'n'=>'n', 'n'=>'n', 'n'=>'n', '?'=>'n', '?'=>'n', 'ð'=>'o', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'o'=>'o', 'o'=>'o', 'o'=>'o', 'œ'=>'o', 'ø'=>'o', 'r'=>'r', 'r'=>'r', 's'=>'s', 's'=>'s', 's'=>'s', 'š'=>'s', '?'=>'s', 't'=>'t', 't'=>'t', 't'=>'t', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'u'=>'u', 'w'=>'w', 'ý'=>'y', 'ÿ'=>'y', 'y'=>'y', 'z'=>'z', 'z'=>'z', 'ž'=>'z'
	));
	//Suppression des accents
	$string = str_replace(' ', '', $string);
    return $string;
}

function adjustNavigation($page, $i, $numberOfPages) {
	$docNav = new DOMDocument('1.0', 'UTF-8');
	$root = $docNav->createElement('navigation', "");
	$docNav->appendChild($root);	
	// first link management
	if ($i != 0) {
		$node = $docNav->createElement("firstlink");
		$node->setAttribute("url", "index.html");
		$root->appendChild($node);	
	}
	// last link
	if ($i != $numberOfPages-1) {
		$node = $docNav->createElement("lastlink");
		$node->setAttribute("url", "index".($numberOfPages-1).".html");
		$root->appendChild($node);	
	}
	// first link management
	if ($i > 0) {
		$prevPage = $i - 1;
		$node = $docNav->createElement("prevlink");
		if ($prevPage == 0) {
			$node->setAttribute("url", "index.html");
		} else {
			$node->setAttribute("url", "index".$prevPage.".html");
		}
		$root->appendChild($node);	
	}
	// last link
	if ($i < $numberOfPages-1) {
		$nextPage = $i + 1;
		$node = $docNav->createElement("nextlink");
		$node->setAttribute("url", "index".$nextPage.".html");
		$root->appendChild($node);	
	}
	
	for ($j=0; $j < $numberOfPages; $j++) { 
		$node = $docNav->createElement("pagelink");
		if ($i != $j) {
			if ($j == 0) {
				$node->setAttribute("url", "index.html");
			} else {
				$node->setAttribute("url", "index".$j.".html");
			}
		}
		$node->setAttribute("pagenum", $j+1);
		$root->appendChild($node);	
	}
	return $docNav;
}

function getCatDom($cat) {
	$docNav = new DOMDocument('1.0', 'UTF-8');
	$root = $docNav->createElement('catlist', "");
	$docNav->appendChild($root);	
	foreach ($cat as $key => $value) {
		$node = $docNav->createElement("cat");
		$node->setAttribute("name", $key);
		$node->setAttribute("url", $value);
		$root->appendChild($node);	
	}
	return $docNav;
}

?>