function showLightBox()
{
	$("#lightbox-container img").attr('src', $(this).attr('src'));
	$("#lightbox-container").fadeIn();
	
	var padding = $("#lightbox-img").css('padding').split('p')[0];
	var marginLeft = $("#lightbox-img").width()/2;
	var marginTop = $("#lightbox-img").height()/2;
	
	$("#lightbox-img").css('margin-left', '-'+marginLeft-padding+'px');
	$("#lightbox-img").css('margin-top', '-'+marginTop-padding+'px');
	
	$("#lightbox-container").click(function(){
		$(this).fadeOut();
	});
}
$(document).ready(function(){
//handlers for the detail-page
$('.lightbox').on('click', showLightBox); });
